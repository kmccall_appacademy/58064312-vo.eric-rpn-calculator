class RPNCalculator
  def initialize(value = 0, array = [])
    @value = value
    @array = array
  end

  def push(n)
    @array << n
  end

  def value
    @value
  end

  def error
    raise "calculator is empty" if @array.length < 2
  end

  def plus
    self.error
    @value =  @array[-1] + @array[-2]
    @array.pop(2)
    @array.push(@value)
  end

  def minus
    self.error
    @value = @array[-2] - @array[-1]
    @array.pop(2)
    @array.push(@value)
  end

  def times
    self.error
    @value = @array[-2] * @array[-1]
    @array.pop(2)
    @array.push(@value)
  end

  def divide
    self.error
    @value = @array[-2].to_f / @array[-1].to_f
    @array.pop(2)
    @array.push(@value)
  end

  def tokens(string)
    str.split.map do |char|
      if char.is_a? !Integer
        char.to_sym
      end
    end
  end
end
